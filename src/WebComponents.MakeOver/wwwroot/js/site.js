﻿window.addEventListener('WebComponentsReady', function() {
  let uri = 'ws://' + window.location.host + '/chat';
  let socket = null;
  function connect() {
    socket = new WebSocket(uri);
    socket.onmessage = function (event) {
      var envelope = JSON.parse(event.data);
      if (envelope.type === 'join') {
        appendParticipant(envelope.id);
      } else if (envelope.type === 'left') {
        removeParticipant(envelope.id);
      } else {
        appendMessage(envelope.id, envelope.message);
      }
    };
  }
  connect();

  let messages = document.getElementById('messages');
  let participants = document.getElementById('participants');
  let button = document.getElementById('sendButton');
  button.addEventListener('click', function () {
    let input = document.getElementById('textInput');
    sendMessage(input.value);
    input.value = '';
  });

  function sendMessage(message) {
    socket.send(message);
  }

  function removeParticipant(id) {
    participants.removeParticipant(id);
  }

  function appendParticipant(id) {
    participants.addParticpant(id);
  }

  function appendMessage(id, message) {
    let item = document.createElement('dt');
    item.classList.add(`c${id}`);
    item.innerHTML = ` ${id} (${new Date().toLocaleTimeString()})`;
    messages.appendChild(item);
    item = document.createElement('dd');
    item.classList.add(`c${id}`);
    item.innerHTML = message;
    messages.appendChild(item);
    item.scrollIntoView();
  }
});
