(function () {
  xtag.register('text-input', {
    content: `
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-size">
        <input class="mdl-textfield__input" type="text">
        <label class="mdl-textfield__label"></label>
      </div>`,

    lifecycle: {
      created() {
        this.xtag.input = this.querySelector('input');
        this.querySelector('label').innerHTML = this.label;
      }
    },

    accessors: {
      label: { attribute: { property: 'input' } },
      value: {
        get() { return this.xtag.input.value; },
        set(value) { this.xtag.input.value = value; }
      }
    },

    methods: {
      blur() {
        this.xtag.input.blur();
      }
    }
  });
}());
