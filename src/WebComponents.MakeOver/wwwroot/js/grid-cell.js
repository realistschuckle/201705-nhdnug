(function () {
  xtag.register('grid-cell', {
    lifecycle: {
      created() {
        this.classList.add('mdl-cell')
        this.classList.add(`mdl-cell--${this.width}-col`);
      }
    },

    accessors: {
      width: { attribute: true }
    }
  });
}());
