(function () {
  xtag.register('flat-color-button', {
    lifecycle: {
      created() {
        let html = this.innerHTML;
        this.innerHTML = `<button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">${html}</button>`;
      }
    }
  });
}());
