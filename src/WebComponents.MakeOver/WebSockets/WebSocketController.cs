using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WebComponents.MakeOver.WebSockets
{
  public abstract class WebSocketController
  {
    public WebSocketController(WebSocketCollection collection)
    {
      _collection = collection;
    }

    public abstract Task ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer);

    public virtual async Task OnConnectedAsync(WebSocket socket)
    {
      _collection.AddSocket(socket);
      await Task.CompletedTask;
    }

    public virtual async Task OnDisconnectedAsync(WebSocket socket)
    {
      await _collection.RemoveSocket(socket);
    }

    public async Task SendMessageAsync(WebSocket socket, object o)
    {
      var json = JsonConvert.SerializeObject(o);
      await SendMessageAsync(socket, json);
    }

    public async Task SendMessageAsync(WebSocket socket, string message)
    {
      if (socket.State == WebSocketState.Open)
      {
        var bytes = Encoding.UTF8.GetBytes(message);
        var buffer = new ArraySegment<byte>(bytes, 0, bytes.Length);
        var messageType = WebSocketMessageType.Text;
        var token = CancellationToken.None;
        await socket.SendAsync(buffer, messageType, true, token);
      }
    }

    public async Task SendMessageAsync(string socketId, string message)
    {
      var socket = _collection.GetSocketById(socketId);
      await SendMessageAsync(socket, message);
    }

    public async Task SendMessageToAllAsync(string message)
    {
      foreach (var socket in _collection.GetAll())
      {
        if (socket.State == WebSocketState.Open)
        {
          await SendMessageAsync(socket, message);
        }
      }
    }

    public async Task SendMessageToAllAsync(object o)
    {
      var json = JsonConvert.SerializeObject(o);
      await SendMessageToAllAsync(json);
    }

    protected WebSocketCollection Sockets => _collection;

    private readonly WebSocketCollection _collection;
  }
}
