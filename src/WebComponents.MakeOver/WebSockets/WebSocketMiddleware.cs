using System;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace WebComponents.MakeOver.WebSockets
{
  public class WebSocketMiddleware
  {
    public WebSocketMiddleware(RequestDelegate next, WebSocketController controller)
    {
      _next = next;
      _controller = controller;
    }

    public async Task Invoke(HttpContext context)
    {
      if (context.WebSockets.IsWebSocketRequest)
      {
        var socket = await context.WebSockets.AcceptWebSocketAsync();
        await _controller.OnConnectedAsync(socket);

        var bytes = new byte[1024 * 4];
        var buffer = new ArraySegment<byte>(bytes);
        while (socket.State == WebSocketState.Open)
        {
          var token = CancellationToken.None;
          var result = await socket.ReceiveAsync(buffer, token);

          if (result.MessageType == WebSocketMessageType.Text)
          {
            await _controller.ReceiveAsync(socket, result, bytes);
          }
          else if (result.MessageType == WebSocketMessageType.Close)
          {
            await _controller.OnDisconnectedAsync(socket);
          }
        }
      }
    }

    private readonly RequestDelegate _next;
    private readonly WebSocketController _controller;
  }
}
