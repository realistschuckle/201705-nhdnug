using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;

namespace WebComponents.MakeOver.WebSockets
{
  public class WebSocketCollection
  {
    public WebSocketCollection()
    {
      _sockets = new ConcurrentDictionary<string, WebSocket>();
    }

    public WebSocket GetSocketById(string id)
    {
      return _sockets.FirstOrDefault(p => p.Key == id).Value;
    }

    public IEnumerable<WebSocket> GetAll()
    {
      return _sockets.Select(p => p.Value);
    }

    public string GetId(WebSocket socket)
    {
      return _sockets.FirstOrDefault(p => p.Value == socket).Key;
    }

    public void AddSocket(WebSocket socket)
    {
      _sockets.TryAdd(CreateConnectionId(), socket);
    }

    public async Task RemoveSocket(WebSocket socket)
    {
      await RemoveSocket(GetId(socket));
    }

    public async Task RemoveSocket(string id)
    {
      _sockets.TryRemove(id, out WebSocket socket);
      await socket.CloseAsync(closeStatus: WebSocketCloseStatus.NormalClosure,
                              statusDescription: "Closed by the WebSocketManager",
                              cancellationToken: CancellationToken.None);
    }

    private string CreateConnectionId()
    {
      return Guid.NewGuid().ToString();
    }

    private readonly ConcurrentDictionary<string, WebSocket> _sockets;
  }
}
