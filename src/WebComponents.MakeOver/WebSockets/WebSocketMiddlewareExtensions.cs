using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace WebComponents.MakeOver.WebSockets
{
  public static class WebSocketMiddlewareExtensions
  {
    public static IApplicationBuilder MapWebSocketController(this IApplicationBuilder app, PathString path, WebSocketController controller)
    {
      return app.Map(path, a => a.UseMiddleware<WebSocketMiddleware>(controller));
    }

    public static IServiceCollection AddWebSocketControllers(this IServiceCollection services)
    {
      services.AddTransient<WebSocketCollection>();

      foreach (var type in Assembly.GetEntryAssembly().ExportedTypes)
      {
        if (typeof(WebSocketController).IsAssignableFrom(type) && type != typeof(WebSocketController))
        {
          services.AddSingleton(type);
        }
      }

      return services;
    }
  }
}
