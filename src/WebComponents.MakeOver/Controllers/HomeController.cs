﻿using Microsoft.AspNetCore.Mvc;

namespace WebComponents.MakeOver.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
