using System;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;
using WebComponents.MakeOver.WebSockets;

namespace WebComponents.MakeOver.Controllers
{
  public class ChatController : WebSocketController
  {
    public ChatController(WebSocketCollection collection)
      : base(collection)
    {
    }

    public override async Task OnConnectedAsync(WebSocket socket)
    {
      await base.OnConnectedAsync(socket);

      var socketId = Sockets.GetId(socket);

      var allIds = Sockets.GetAll().Select(s => Sockets.GetId(s));
      foreach (var id in allIds)
      {
        await SendMessageAsync(socket, new { type = "join", id = id });
      }
      await SendMessageToAllAsync(new { type = "join", id = socketId });
    }

    public override async Task OnDisconnectedAsync(WebSocket socket)
    {
      var socketId = Sockets.GetId(socket);

      await base.OnDisconnectedAsync(socket);

      await SendMessageToAllAsync(new { type = "left", id = socketId });
    }

    public override async Task ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer)
    {
      var socketId = Sockets.GetId(socket);
      await SendMessageToAllAsync(new
      {
        type = "message",
        id = socketId,
        message = Encoding.UTF8.GetString(buffer, 0, result.Count) }
      );
    }
  }
}
